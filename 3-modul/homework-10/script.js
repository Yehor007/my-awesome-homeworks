//? Теоретичні питання
//? 1. Які способи JavaScript можна використовувати для створення та додавання нових DOM-елементів?
//! document.createElement(tag), document.createTextNode(text).
//? 2. Опишіть покроково процес видалення одного елементу (умовно клас "navigation") зі сторінки.
//! Визначаємо елемент, який ми хочемо видалити(const navigationElement = document.querySelector('.navigation');),
//! Перевіряємо, чи цей елемент є існуючим,щоб не було помилок,
//! Використовую метод remove() для видалення елементу з DOM.
//! Перевіряю результат.
//? 3. Які є методи для вставки DOM-елементів перед/після іншого DOM-елемента?
//! node.append, node.prepend, node.before, node.after.
"use strict";
//? Практичні завдання
//? 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.
const newEl = document.createElement("a");
const text = document.querySelector(".txt");
newEl.setAttribute("href", "#");
newEl.textContent = "Learn More";
text.after(newEl);
console.log(newEl);
//? 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
//? Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
//? Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
//? Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
//? Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const newSelect = document.createElement("select");
newSelect.id = "rating";
console.log(newSelect);
const main = document.querySelector("main");
main.prepend(newSelect);

const optionsData = [
  { value: "4", text: "4 Stars" },
  { value: "3", text: "3 Stars" },
  { value: "2", text: "2 Stars" },
  { value: "1", text: "1 Star" },
];

optionsData.forEach((optionData) => {
  const option = document.createElement("option");
  option.value = optionData.value;
  option.textContent = optionData.text;
  newSelect.appendChild(option);
});
