//? Теоретичні питання
//? 1. Що таке події в JavaScript і для чого вони використовуються?
//! Подія - це сигнал, який свідчить, що щось відбулось.Події використовуються для відслідковування та
//! реагування на дії користувача.
//? 2. Які події миші доступні в JavaScript? Наведіть кілька прикладів.
//! click, contextmenu, mouseover, mouseout, mousedown, mouseup.
//! element.addEventListener('click', function() {
      //* Обробка події click
//!   });

//! element.addEventListener('mouseout', function() {
    //* Обробка події mouseout
//!   });

//! element.addEventListener('mouseup', function() {
    //* Обробка події mouseup
//! });
//? 3. Що таке подія "contextmenu" і як вона використовується для контекстного меню?
//! contextmenu - це подія, яка відбувається, коли користувач правою кнопкою миші клікає на елемент.
//! Зазвичай використовується для перешкодження стандартної поведінки браузера, яка включає відображення
//! стандартного контекстного меню при правому кліку.
'use srtict';
//? Практичні завдання
//?  1. Додати новий абзац по кліку на кнопку:
//?   По кліку на кнопку <button id="btn-click">Click Me</button>, створіть новий елемент <p> з текстом "New Paragraph" і додайте його до розділу <section id="content">
const btn = document.querySelector("#btn-click");
btn.addEventListener('click', function(){
    const paragraph = document.createElement("p");
    paragraph.textContent = 'New Paragraph';
    const section = document.querySelector("#content");
    section.prepend(paragraph);
});
//?  2. Додати новий елемент форми із атрибутами:
//?  Створіть кнопку з id "btn-input-create", додайте її на сторінку в section перед footer.
//?   По кліку на створену кнопку, створіть новий елемент <input> і додайте до нього власні атрибути, наприклад, type, placeholder, і name. та додайте його під кнопкою.
const button = document.createElement("button");
button.id = 'btn-input-create';
console.log(button);
const section = document.querySelector("#content");
section.prepend(button);
button.style.width = '48px';
button.style.height = '36px';
button.addEventListener('click', function(){
    const input = document.createElement("input");
    input.setAttribute('type', 'text');
    input.setAttribute('placeholder', 'Введіть текст');
    input.setAttribute('name', 'userInput');
    input.style.display = 'block';
    input.style.marginLeft = 'auto';
    input.style.marginRight = 'auto';
    input.style.marginTop = '5px';
    button.after(input);
})


 


