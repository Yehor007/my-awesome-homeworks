/* Теоретичні питання
1. Яке призначення методу event.preventDefault() у JavaScript?

Запобігання діям браузера. Ми використовуємо цей метод коли не хочемо, щоб в браузері відбувалася якась подія.

2. В чому сенс прийому делегування подій?

Якщо ми маємо елементи, які обробляються однаковим чином, то для того щоб не призначати обробник
для кожного з них, ми встановлюємо один обробник на їхнього спільного предка.

3. Які ви знаєте основні події документу та вікна браузера? 

document.addEventListener('DOMContentLoaded', function() {})
window.addEventListener('load', function() {})
window.addEventListener('onunload', function() {})
window.addEventListener('onload', function() {})
window.addEventListener('onbeforeunload', function() {})


Практичне завдання:

Реалізувати перемикання вкладок (таби) на чистому Javascript.

Технічні вимоги:

- У папці tabs лежить розмітка для вкладок. Потрібно, щоб після натискання на вкладку відображався конкретний текст для потрібної вкладки. При цьому решта тексту повинна бути прихована. У коментарях зазначено, який текст має відображатися для якої вкладки.
- Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.
- Потрібно передбачити, що текст на вкладках може змінюватись, і що вкладки можуть додаватися та видалятися. При цьому потрібно, щоб функція, написана в джаваскрипті, через такі правки не переставала працювати.

 Умови:
 - При реалізації обов'язково використовуйте прийом делегування подій (на весь скрипт обробник подій повинен бути один).
*/

"use strict";
document.addEventListener("DOMContentLoaded", function () {
  const tabsContainer = document.querySelector(".tabs");
  const tabsTitles = document.querySelectorAll(".tabs-title");
  const tabsContent = document.querySelectorAll(".tabs-content li");

  tabsContainer.addEventListener("click", function (event) {
    const clickedTab = event.target;
    if (clickedTab.classList.contains("tabs-title")) {
      tabsTitles.forEach((tab) => tab.classList.remove("active"));
      tabsContent.forEach((tab) => (tab.style.display = "none"));
      clickedTab.classList.add("active");
      const tabIndex = Array.from(tabsTitles).indexOf(clickedTab);
      tabsContent[tabIndex].style.display = "block";
    }
  });
});
