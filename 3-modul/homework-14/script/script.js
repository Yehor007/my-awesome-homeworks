// Теоретичні питання
// 1. В чому полягає відмінність localStorage і sessionStorage?
//! При використанні localStorage дані залишаються після перезавантаження браузера і навіть після його закриття, а при використанні
//! sessionStorage дані зберігаються після оновлення сторінки, але не закриття/відкриття вкладки, також він діє тільки в одній вкладці.
// 2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
//! Непередавати паролі у відкритому вигляді. Треба використовувати методи шифрування.
// 3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?
//! Коли завершується сеанс браузера, дані, збережені в sessionStorage, будуть видалені. Данні зберігаються тільки під час сеансу.


// Практичне завдання:


// Реалізувати можливість зміни колірної теми користувача.



// Технічні вимоги:


// - Взяти готове домашнє завдання HW-5 "Book Shop" з блоку Basic HMTL/CSS.

// - Додати на макеті кнопку "Змінити тему".

// - При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.

// - Вибрана тема повинна зберігатися після перезавантаження сторінки.



// Примітки: 

// - при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;

// - зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.
'use strict';
document.addEventListener('DOMContentLoaded', function () {
    const themeButton = document.querySelector(".theme");
    const section = document.querySelector(".card-section");
    const title = document.querySelector(".card-section__title");
    const card = document.querySelectorAll(".card");
    let darkTheme = localStorage.getItem('darkTheme') === 'true';

    applyTheme();

    themeButton.addEventListener('click', function(){
        darkTheme = !darkTheme;
        localStorage.setItem('darkTheme', darkTheme.toString());
        applyTheme();
    });

    function applyTheme() {
        if (darkTheme) {
            section.classList.add('dark-theme');
            title.classList.add('dark-theme');
            card.forEach(function (cardItem) {
                cardItem.classList.add('dark-theme');
            });
        } else {
            section.classList.remove('dark-theme');
            title.classList.remove('dark-theme');
            card.forEach(function (cardItem) {
                cardItem.classList.remove('dark-theme');
            });
        }
    }
});