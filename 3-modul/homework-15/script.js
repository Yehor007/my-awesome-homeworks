//? Теоретичні питання
//? 1. В чому відмінність між setInterval та setTimeout?
//*Метод setTimeout дозволяє запускати функцію один раз через певний інтервал часу, а метод setInterval запускає функцію багаторазово через заданий проміжок часу.
//? 2. Чи можна стверджувати, що функції в setInterval та setTimeout будуть виконані рівно через той проміжок часу, який ви вказали?
//* Ні, тому що можуть бути затримки.setTimeout дозволяє більш точно встановити затримку між виконанням, ніж setInterval.
//? 3. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?
//* код в функції, виконується після визначеного проміжку часу (у випадку setTimeout) або періодично (у випадку setInterval).

// Практичне завдання:
"use strict";
// -Створіть HTML-файл із кнопкою та елементом div.
// -При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.
document.addEventListener("DOMContentLoaded", function () {
  const changeTextButton = document.getElementById("change-text-button");
  const outputText = document.getElementById("output-text");

  changeTextButton.addEventListener("click", function () {
    setTimeout(function () {
      outputText.textContent = "Операція виконана успішно!";
    }, 3000);
  });
});
// Практичне завдання 2:
// Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
document.addEventListener("DOMContentLoaded", function () {
  const countdownElement = document.getElementById("countdown");
  let countdownValue = 10;

  function updateCountdown() {
    countdownElement.textContent = countdownValue;

    if (countdownValue === 0) {
      clearInterval(countdownInterval);
      countdownElement.textContent = "Зворотній відлік завершено";
    } else {
      countdownValue--;
    }
  }
  const countdownInterval = setInterval(updateCountdown, 1000);
});
