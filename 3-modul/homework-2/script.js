//? 1. Що таке оператори в JavaScript і які їхні типи ви знаєте?
//! Оператори в js - це символи за допомогою яких виконуються операції над операндами.
//! Типи операторів:Унарні та Бінарні, оператори порівняння,арифметичні,логічні,
//? 2. Для чого використовуються оператори порівняння в JavaScript? Наведіть приклади таких операторів.
//! Оператори порівняння використовуються для того, щоб порівняти два значення. Є такі оператори порівняння
//! <, >, <=, >=, ==, !=, ===, !==.
//? 3. Що таке операції присвоєння в JavaScript? Наведіть кілька прикладів операцій присвоєння.
//! Операції присвоєння - це надання змінним якогось значення. 
//! let number = 12;
//! const uName = "Alex";

//? 1. Створіть змінну "username" і присвойте їй ваше ім'я. 
//? Створіть змінну "password" і присвойте їй пароль (наприклад, "secret").
//? Далі ми імітуємо введеня паролю користувачем. Отримайте від користувача значення його паролю 
//? і перевірте, чи співпадає воно зі значенням в змінній "password". Виведіть результат порівнння в консоль.
"use strict";
const userName = "Yehor";
const passWord = "secret";
let userInput = prompt("Введіть пароль,будь ласка");
console.log(userInput === passWord);
//? 2. Створіть змінну x та присвойте їй значення 5. Створіть ще одну змінну y та запишіть 
//? присвойте їй значення 3.
//? Виведіть результати додавання, віднімання, множення та ділення
//? змінних x та y у вікні alert.
const x = 5;
const y = 3;
alert (x + y);
alert (x - y);
alert (x * y);
alert (x / y);