//? Теоретичні питання
//? 1. Що таке цикл в програмуванні?
//! Це структура завдяки якій ми можемо виконувати повторно блок коду,поки умова є істиною.
//? 2. Які види циклів є в JavaScript і які їх ключові слова?
//! Цикл for, while, do while.
//? 3. Чим відрізняється цикл do while від while?
//! Оператор while - оцінює вираз перед кожною ітерацією циклу
//! Цикл do while завжди виконує оператор хоча б 1 раз, тому що він оцінює вираз після кожної ітерації.

//? Практичні завдання
//? 1. Запитайте у користувача два числа.
//? Перевірте, чи є кожне з введених значень числом.
//? Якщо ні, то запитуйте у користувача нове занчення до тих пір, поки воно не буде числом.
//? Виведіть на екран всі цілі числа від меншого до більшого за допомогою циклу for.

'use strict';
// let number1,number2;
// do{
//     number1 = prompt("Please enter your first number:");
// } while(isNaN(number1));
// do{
//     number2 = prompt("Please enter your second number:");
// }while(isNaN(number2));
// number1 = parseInt(number1);
// number2 = parseInt(number2);
// for (let i = Math.min(number1, number2); i <= Math.max(number1, number2); i++) {
//     console.log(i);
// }

//? 2. Напишіть програму, яка запитує в користувача число та перевіряє, чи воно є парним числом. 
//? Якщо введене значення не є парним числом, то запитуйте число доки користувач не введе правильне значення.

let someNumber;
do {
    someNumber = prompt("Insert some number:")
} while (isNaN(someNumber) || someNumber %2 !==0 && someNumber !== null);
console.log(someNumber);