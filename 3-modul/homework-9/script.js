//? Теоретичні питання
//? 1. Опишіть своїми словами що таке Document Object Model (DOM)
//! Це програмний інтерфейс,який показує структуру документа у вигляді дерева об'єктів.
//? 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
//! `innerHTML`використовується , коли потрібно маніпулювати вмістом всередині елемента.
//! `innerText`використовується , коли потрібно маніпулювати текстовим вмістом з урахуванням стилізації 
//! CSS та можливого виключення прихованих елементів.
//? 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
//! Є кілька способів звернення до елемента за допомогою JS:
//! document.querySelector();
//! document.querySelectorAll();
//! document.getElementsByName();
//! document.getElementById();
//! document.getElementsByClassName();
//! document.getElementsByTagName();
//! Сказати який спосіб краще важко, тому що вибір способу для використання залежить від конкретної ситуації
//! Зазвичай, використовують комбінацію цих методів в залежності від вимог. getElement методи 
//! зазвичай мають кращу продуктивність, а етоди типу `querySelector` забезпечують баланс між пецифічністю
//! та гнучкістю.
//? 4. Яка різниця між nodeList та HTMLCollection?
//! NodeList - це колекція вузлів, які повертаються методами querySelector. 
//! HTMLCollection - це колекція HTML-елементів,повертається методами
//! `getElementsByTagName` чи `getElementsByClassName`.
//! HTMLCollection трохи більш обмежена, ніж `NodeList`,оскільки містить тільки елементи.
//! NodeList є більш загальною і може включати будь-який тип вузла, a `HTMLCollection` специфічно стосується HTML-елементів.
//? Практичні завдання
//? 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//? Використайте 2 способи для пошуку елементів.
//? Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).
//* const arcticleClass = document.getElementsByClassName("feature");
//* console.log("1 спосіб");
//* console.log(arcticleClass);
//* const article = document.querySelectorAll(".feature");
//* console.log("2 спосіб");
//* console.log(article);
//* article.forEach(function(element) {
//*     element.style.textAlign = 'center';
//*   });
//? 2. Змініть текст усіх елементів h2 на "Awesome feature".
//* const h2 = document.querySelectorAll('h2');
//* h2.forEach(function(element) {
//*   element.textContent = 'Awesome feature';
//* });
//? 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
//* const title = document.querySelectorAll(".feature-title");
//* title.forEach(function(element) {
//*     element.textContent += '!';
//*   });