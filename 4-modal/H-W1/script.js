//? Теоретичне питання

//? Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript
//! В JS об'єкт може успадковувати методи та властивості іншого об'єкта, який є прототипом
//! Коли є звернення до властивості або методу об'єкта, JavaScript спочатку шукає його власні властивості та методи.
//! У випадку, якщо він їх не знаходить, то він переходить до пошуку в прототипі цього об'єкта.
//!  Якщо властивість або метод також не знайдений у прототипі, JavaScript продовжує цей процес вглиб ієрархії прототипів
//! до тих пір, поки не буде знайдено відповідну властивість або метод, або доки не дістанеться кінеця ланцюжка прототипів.
//? Для чого потрібно викликати super() у конструкторі класу-нащадка?
//! Для того, щоб зробити клас успадковуючим від класу батька. Таким чином клас-нащадок успадковує методи і властивості батьківського класу.
//? Завдання
//? Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
//? Створіть гетери та сеттери для цих властивостей.
//? Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
//? Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
//? Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

"use strict";
class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }

  get lang() {
    return this._lang;
  }

  set lang(lang) {
    this._lang = lang;
  }
}

const developer1 = new Programmer("John", 31, 40000, ["JavaScript", "Python"]);
const developer2 = new Programmer("Alice", 25, 60000, ["Java", "C++"]);

console.log(developer1);
console.log(developer2);
