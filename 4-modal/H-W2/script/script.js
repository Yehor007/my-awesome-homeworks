//? Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//! try...catch використовується тоді, коли ми хочемо обробляти помилку і продовжувати виконання.
//! Ми можемо його використовувати при роботі з DOM:
//! try {
//!  const element = document.getElementById("paragraph");
//!  element.innerHTML = "Hello, world!";
//! } catch (error) {
//!  console.error("Элемент не знайдено:", error.message);
//! }
//! При роботі з введенням користувача також можна використати try...catch
//! function checkUserName(name) {
//!   try {
//!     if (typeof name !== "string") {
//!       throw new Error("Введіть ім'я правильно");
//!     }
//!   } catch (error) {
//!     console.error("Помилка!", error.message);
//!   }
//! }
//? Завдання
//? Дано масив books.

//? Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
//? На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
//? Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author,
//? name, price). Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
//? Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.getElementById("root");
const ul = document.createElement("ul");
root.appendChild(ul);

books.forEach((goods, index) => {
  try {
    if (!goods.author) {
      throw new Error(`Немає автора книги ${index}`);
    }
    if (!goods.name) {
      throw new Error(`Немає назви книги${index}`);
    }
    if (!goods.price) {
      throw new Error(`Немає ціни у книги ${index}`);
    }
    const li = document.createElement("li");
    ul.appendChild(li);
    li.textContent = `${goods.author}, ${goods.name}, ${goods.price} грн`;
  } catch (error) {
    console.error(error.message);
  }
});
