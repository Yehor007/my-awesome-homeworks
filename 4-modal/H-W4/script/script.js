//? Теоретичне питання
//? Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
//! Це технологія, яка дозволяє нам виконувати асинхронні запити до сервера.
//! Корисний тим, що дозволяє отримувати дані від веб-сервера без перезавантаження сторінки. Завдяки ньому
//! ми можемо створювати швидкі та інтерактивні веб-застосунки.

//? Завдання
//? Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

//? Технічні вимоги:
//? Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
//? Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
//? Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
//? Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

fetch("https://ajax.test-danit.com/api/swapi/films")
  .then((response) => response.json())
  .then((films) => {
    const root = document.getElementById("root");

    films.forEach((film) => {
      const div = document.createElement("div");
      div.classList.add("film");

      div.innerHTML = `
        <h2>Епізод ${film.episodeId}: ${film.name}</h2>
        <p>${film.openingCrawl}</p>
        <h3>Персонажі:</h3>
        <ul id="characters-${film.id}">Завантаження...</ul>
      `;
      root.appendChild(div);

      const characterPromises = film.characters.map((characterUrl) =>
        fetch(characterUrl).then((res) => res.json())
      );

      Promise.all(characterPromises).then((characters) => {
        const characterList = document.getElementById(`characters-${film.id}`);
        characterList.innerHTML = "";

        characters.forEach((character) => {
          const li = document.createElement("li");
          li.textContent = character.name;
          characterList.appendChild(li);
        });
      });
    });
  })
  .catch((error) => {
    console.log("Помилка при завантаженні даних:", error);
  });
