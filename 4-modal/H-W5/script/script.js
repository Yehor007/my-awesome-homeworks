//? Завдання
//? Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
//? Технічні вимоги:
//? При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
//? https://ajax.test-danit.com/api/json/users
//? https://ajax.test-danit.com/api/json/posts
//? Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
//? Кожна публікація має бути відображена у вигляді картки (приклад в папці), та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
//? На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки. При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}. Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
//? Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
//? Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
//? Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.

class Card {
  constructor(post, user) {
    this.post = post;
    this.user = user;
  }

  createCard() {
    const card = document.createElement("div");
    card.classList.add("card");

    card.innerHTML = `
            <h2>${this.post.title}</h2>
            <p>${this.post.body}</p>
            <p><strong>${this.user.name}</strong> (${this.user.email})</p>
            <button class="delete-button">Delete</button>
        `;

    card.querySelector(".delete-button").addEventListener("click", () => {
      this.deleteCard(card);
    });

    return card;
  }

  deleteCard(cardElement) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${this.post.id}`, {
      method: "DELETE",
    })
      .then((response) => {
        if (response.status < 300) {
          cardElement.remove();
        } else {
          alert("Failed to delete the post.");
        }
      })
      .catch((error) => console.log("Error:", error));
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const root = document.getElementById("root");

  Promise.all([
    fetch("https://ajax.test-danit.com/api/json/users").then((res) =>
      res.json()
    ),
    fetch("https://ajax.test-danit.com/api/json/posts").then((res) =>
      res.json()
    ),
  ])
    .then(([users, posts]) => {
      posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        const card = new Card(post, user);
        root.appendChild(card.createCard());
      });
    })
    .catch((error) => console.log("Error:", error));
});
