//? Теоретичне питання
//? Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript
//! Це дії в JS, які ми можемо виконувати незалежно від основного коду, не конфліктуючи з ним.
//? Завдання
//? Написати програму "Я тебе знайду по IP"

//? Технічні вимоги:
//? Створити просту HTML-сторінку з кнопкою Знайти по IP.
//? Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
//? Дізнавшись IP адресу, надіслати запит на сервіс http://ip-api.com/ та отримати інформацію про фізичну адресу.
//? під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
//? Усі запити на сервер необхідно виконати за допомогою async await.

document.getElementById("ip-btn").addEventListener("click", async () => {
  try {
    const ipResp = await fetch("https://api.ipify.org/?format=json");
    if (ipResp.status < 200 || ipResp.status >= 300) {
      throw new Error(
        `Помилка при отриманні IP-адреси: ${ipResp.status} ${ipResp.statusText}`
      );
    }
    const ipData = await ipResp.json();
    const ipPlace = ipData.ip;

    const locationResp = await fetch(`http://ip-api.com/json/${ipPlace}`);
    if (locationResp.status < 200 || locationResp.status >= 300) {
      throw new Error(
        `Помилка при отриманні інформації про місцезнаходження: ${locationResp.status} ${locationResp.statusText}`
      );
    }
    const location = await locationResp.json();
    console.log(location);
    let root = document.getElementById("root");
    root.innerHTML = `
        <p><strong>IP Адреса:</strong> ${ipPlace}</p>
        <p><strong>Континент:</strong> ${location.timezone}</p>
        <p><strong>Країна:</strong> ${location.country}</p>
        <p><strong>Регіон:</strong> ${location.region}</p>
        <p><strong>Місто:</strong> ${location.city}</p>
        <p><strong>Район:</strong> ${location.regionName}</p>
      `;
  } catch (error) {
    console.log("Помилка:", error);
    root.innerText = `Не вдалося отримати інформацію. Помилка: ${error.message}`;
  }
});
